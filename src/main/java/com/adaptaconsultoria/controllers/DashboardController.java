package com.adaptaconsultoria.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.adaptaconsultoria.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.models.Account;
import com.adaptaconsultoria.models.Stat;
import com.adaptaconsultoria.services.AccountService;
import com.adaptaconsultoria.services.DashboardService;
import com.adaptaconsultoria.services.SessionService;
import com.adaptaconsultoria.utils.pages.PageUtil;

@Controller
@RequestMapping(value = {"dashboard", ""})
public class DashboardController {

	@Autowired
	private SessionService sessionService;

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private AccountService accountService;

	@GetMapping()
	public ModelAndView dashboard(HttpServletRequest request, HttpSession session, Device device) {
		String path = request.getServletPath();
		sessionService.setUser(session);
		if (path.equals("/")) {
			path = "/dashboard";
		}
		List<Stat> stats = dashboardService.list();
		
		Account account = (Account) accountService.getAcccount(session);
		User user = this.accountService.mergeToAccount(this.sessionService.getUser(session), account);
		this.sessionService.updateUser(user, session);

		PageUtil pageUtil = null;
		Boolean isMobile = false;

		if (device.isMobile()) {
			isMobile = true;
			pageUtil = new PageUtil(new ModelAndView("/dashboard-mobile"));
			List<Stat> statsForMobile = new ArrayList<>();
			String saldo = new String();
			for (Stat s : stats) {
				if (!s.getName().equals("Saldo"))
					statsForMobile.add(s);
				else {
					saldo = s.getValue();
				}
			}
			
			stats = statsForMobile;
			pageUtil.setAttr("currency", account.getCurrency());
			pageUtil.setAttr("saldo", saldo);
		} else {
			pageUtil = new PageUtil(new ModelAndView(path));
		}
		
		this.sessionService.getUser(session);
		
		sessionService.setAtribute("isMobile", isMobile, session);
		pageUtil.setPageTitle("Dashboard");
		pageUtil.setTitle("Dashboard");
		sessionService.setProjectName(session);
		
		pageUtil.setAttr("URL", request.getRequestURL().toString().split(request.getRequestURI())[0]);
		pageUtil.setAttr("mi", "dashboard");

		pageUtil.setAttr("stats", stats);
		pageUtil.setJs("dashboard.js");
		accountService.getIndicatorUrl(session, request);
		return pageUtil.getModel();
	}

	@GetMapping("getindicatorurl")
	@ResponseBody
	public ResponseEntity<?> getAccount(HttpServletRequest request, HttpSession session) {
		return ResponseEntity.ok(accountService.getIndicatorUrl(session, request));
	}
}
