package com.adaptaconsultoria.controllers.freely;

import com.adaptaconsultoria.services.CbcService;
import com.adaptaconsultoria.utils.pages.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErroController implements ErrorController {

	@Autowired
	private CbcService cbcService;

	@RequestMapping("/error")
	public ModelAndView handleError() {
		PageUtil pageUtil = new PageUtil(new ModelAndView("/freely/page-404"));
		pageUtil.setAttr("projectName", cbcService.getName());
		return pageUtil.getModel();
	}

	@Override
	public String getErrorPath() {
		return "/freely/error";
	}
}
