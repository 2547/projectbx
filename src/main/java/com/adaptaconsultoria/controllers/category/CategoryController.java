package com.adaptaconsultoria.controllers.category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.services.CategoryService;
import com.adaptaconsultoria.utils.pages.PageUtil;

@Controller
@RequestMapping(value = "category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@GetMapping(value = "category-list-mobile")
	public ModelAndView list(HttpServletRequest request, HttpSession session) {
		PageUtil pageUtil = new PageUtil(new ModelAndView("/category/category-list-mobile"));
		pageUtil.setPageTitle("Categorias");
		pageUtil.setTitle("Categorias");
		pageUtil.setSubTitle("Categorias");
		pageUtil.setJs("category-mobile-list.js");
		return pageUtil.getModel();
	}
	
	@PostMapping(value = "getcategories")
	public ResponseEntity<?> getCategories(Boolean cityCheckbox, HttpSession session) {
		return ResponseEntity.ok(this.categoryService.getSellerCategory(session, cityCheckbox));
	}
}
