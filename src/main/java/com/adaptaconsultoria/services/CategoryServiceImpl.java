package com.adaptaconsultoria.services;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.adaptaconsultoria.objects.in.CategoriesIn;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private RequestService requestService;
	
	@Autowired
	private JsonService jsonService;
	
	@Autowired
	private SessionService sesssionService;
	
	@Autowired
	private UserService userService;
	
	private static final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);
	private static final String pathseller = "seller/category";
	private static final String pathpartner = "partner/category";
	
//	seller?token=32C0D7B76844D600B5361B15D71ED0D2C2C00D68158D5E2E9EC6B0EB122F5D38&ipAddress=localhost&codeCategory=1&cityCode=2
	
	@Override
	public Object getSellerCategoryByCompany(HttpSession session) {
		try {
			Object o = requestService.getRequest(pathseller, true, null);
			CategoriesIn objOp = (CategoriesIn) jsonService.objToObj(o, new CategoriesIn());
			System.out.println(objOp.getCategories());
			return objOp.getCategories();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	@Override
	public Object getPartnerCategoryByCompany(HttpSession session) {
		try {
			Object o = requestService.getRequest(pathpartner, true, null);
			CategoriesIn objOp = (CategoriesIn) jsonService.objToObj(o, new CategoriesIn());
			return objOp.getCategories();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Object getSellerCategoryByCompanyAndCityCodeAndQuery(HttpSession session, String cityCode, String query) {
		try {
			MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
			map.add("cityCode", this.sesssionService.getUser(session).getAddressCityCode());
			map.add("query", query);
			
			Object o = requestService.getRequest(pathseller, true, map);
			CategoriesIn objOp = (CategoriesIn) jsonService.objToObj(o, new CategoriesIn());
			System.out.println(objOp.getCategories());
			return objOp.getCategories();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Object getSellerCategory(HttpSession session, Boolean cityCheckbox) {
		if (cityCheckbox) {
			return getSellerCategoryByCompanyAndCityCodeAndQuery(session, this.userService.getUser(session).getAddressCityCode(),null );
		} else {
			return getSellerCategoryByCompany(session);
		}
	}
}
