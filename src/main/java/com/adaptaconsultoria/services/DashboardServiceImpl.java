package com.adaptaconsultoria.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.models.Stat;
import com.adaptaconsultoria.objects.in.StatsIn;

@Service
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private RequestService requestService;

	@Autowired
	private JsonService jsonService;

	private static final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);
	private static final String url = "dashboard/stats";

	@Override
	public List<Stat> list() {
		try {
			Object o = requestService.getRequest(url, true, null);
			StatsIn objOp = (StatsIn) jsonService.objToObj(o, new StatsIn());
			return objOp.getStats();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
}
