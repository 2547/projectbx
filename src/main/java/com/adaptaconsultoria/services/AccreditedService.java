package com.adaptaconsultoria.services;

import javax.servlet.http.HttpSession;

import com.adaptaconsultoria.models.Accredited;

public interface AccreditedService {

	Object save(Accredited obj, HttpSession session);
	Object list();
	Object findByAccountNo(String accountNo);
	Object findBycodeCategoryAndCityCodeAndQuery(String codeCategory, Boolean cityCode, String query, HttpSession session);
}
