package com.adaptaconsultoria.services;

public interface JsonService {
	public String objToJsonString(Object obj);
	public Object objToObj(Object source, Object dest);
}
