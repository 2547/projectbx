package com.adaptaconsultoria.services;

import com.adaptaconsultoria.models.Account;
import com.adaptaconsultoria.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public interface AccountService {
	public Object getAcccount(HttpSession session);
	public Object findAccount(String query);

	public Object getAccountForEditing(HttpSession session);

	public Object getIndicatorUrl(HttpSession session, HttpServletRequest request);

	public Object getAccountByUserLogin(String login, String token);

	public User mergeToAccount(User user, Account account);
}
