package com.adaptaconsultoria.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.objects.in.UserIn;

@Service
public class TokenServiceImpl implements TokenService {

	@Override
	public void updateToken(String token) {
		try {
			UserIn userIn = new UserIn();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			userIn = (UserIn) auth.getPrincipal();
			userIn.setToken(token);

			List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
			listAuthorities.add(new SimpleGrantedAuthority(getRolePrefix() + userIn.getUser().getRole()));
			Authentication newAuth = new UsernamePasswordAuthenticationToken(userIn, auth.getCredentials(),listAuthorities);

			SecurityContextHolder.getContext().setAuthentication(newAuth);
		} catch (Exception e) {
			
		}
	}

	public String getRolePrefix() {
		return "ROLE_";
	}

	@Override
	public String getToken() {
		try {
			UserIn userIn = new UserIn();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			userIn = (UserIn) auth.getPrincipal();
			return userIn.getToken();
		} catch (Exception e) {
			SecurityContextHolder.clearContext();
			e.printStackTrace();
		}
		return null;
	}

}
