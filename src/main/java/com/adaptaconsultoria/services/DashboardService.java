package com.adaptaconsultoria.services;

import java.util.List;

import com.adaptaconsultoria.models.Stat;

public interface DashboardService {
	public List<Stat> list();
}
