package com.adaptaconsultoria.services;

import java.util.Date;

public interface CashBackService {
	Object list(Date dateFrom, Date dateTo, String url);
}
