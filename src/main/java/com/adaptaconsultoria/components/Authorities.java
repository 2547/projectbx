package com.adaptaconsultoria.components;

import com.adaptaconsultoria.models.User;
import com.adaptaconsultoria.objects.in.UserIn;
import com.adaptaconsultoria.services.CbcService;
import com.adaptaconsultoria.services.LoginService;
import com.adaptaconsultoria.services.SessionService;
import com.adaptaconsultoria.services.UserService;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Component
public class Authorities implements GrantedAuthority  {

	@Autowired
	private SessionService sessionService;

	@Override
	public String getAuthority() {
		User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return principal.getRole();
	}
}
