package com.adaptaconsultoria.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import com.adaptaconsultoria.models.Account;
import com.adaptaconsultoria.models.User;
import com.adaptaconsultoria.services.AccountService;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.adaptaconsultoria.objects.in.UserIn;
import com.adaptaconsultoria.services.CbcService;
import com.adaptaconsultoria.services.LoginService;

@Component
public class CbcAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private LoginService loginService;

	@Autowired
	private CbcService cbcService;

	@Autowired
	private AccountService accountService;

	@Autowired
	ObjectFactory<HttpSession> httpSessionFactory;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try {
			String username = authentication.getName().trim();
			String password = authentication.getCredentials().toString().trim();

			Optional<UserIn> op = null;
			if (username.equals(cbcService.getAppToken())) {
				op = Optional.of(loginService.remoteLogin(password));
			} else {
				op = Optional.of(loginService.login(username.toLowerCase(), password));
			}

			if (op.get().getHasError()) {
				throw new Exception();
			}

			List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
			listAuthorities.add(new SimpleGrantedAuthority(getRolePrefix() + op.get().getUser().getRole()));

				return new UsernamePasswordAuthenticationToken(op.get(), password, listAuthorities);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	
	public String getRolePrefix() {
		return "ROLE_";
	}
}
