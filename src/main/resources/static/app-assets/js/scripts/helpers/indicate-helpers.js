const accounts = '/getindicatorurl';
const base_url = 'dashboard'

$(document).ready(function() {
    getAccount()
    block2(".blockit")
});

function getAccount() {
    $.ajax({
        url: contextPath + base_url + accounts,
        type: "GET",
        success: (obj) => {
            unblock(".blockit")
            $("#indicate").val(obj)
            $(".indicate-href").attr("href", obj);
        }
    });
}