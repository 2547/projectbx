var table;
var ajaxCounter = 0;
const getCategoriesUrl =  contextPath + "category/getcategories";
const listAccreditedsUrl = contextPath + "accredited/list";

var cityCheckbox = true;

$(document).ready(function() {
	onClickCityCheckBox()
	getCategories()
	onClickFilter()
})

function onClickFilter() {
	$("#filter").click(() => {
		let codeCategory = $("input[name='options']:checked").val() ? $("input[name='options']:checked").val() : '';
		let query = $("#query").val();
		let cityCode = cityCheckbox;
		let redirectUrl = listAccreditedsUrl + `?cityCode=${cityCode}&codeCategory=${codeCategory}&query=${query}`
		window.location.href = redirectUrl;
	})
}

function onClickCityCheckBox() {
	$("#cityCheckbox").click((event) => {
		cityCheckbox = $("#cityCheckbox").prop("checked");
		getCategories()
	})
}

function list() {
	var featureList = new List('category-list', { 
		  valueNames: [ 'name', 'cityCode' ]
	});
}

async function getCategories() {
	block2(".blockit")
	let categories = await getCategoriesPromise();
	$("#build-list").html(buildList(categories))
	list()
}

function buildList(categories) {
	var html = "";
	categories.forEach(category => {
		html += `
		<label class="btn btn-primary btn btn-block btn-primary btn-custom btn-social categoria d-flex align-items-center">
			<i> <input type="radio" name="options"  value="${category.code}"></i> 
			<span class="name ml-1"> ${category.name}</span>
		</label>
		`;
	}) 
	
	
	
	return html;
	
	//<a href="${contextPath}accredited/list?code=${category.code}" class="btn btn-block btn-primary btn-custom btn-social categoria"> 
	//<i class="la la-institution "></i> <span class="name"> ${category.name}</span>
    //</a>
}

function getCategoriesPromise() {
	return new Promise(function(resolve, reject) {
		 $.ajax({
		      url: getCategoriesUrl,  
		      type: "POST",
		      data: {
		    	  cityCheckbox: cityCheckbox
		      },
		      success: function(obj) {
		    	  resolve(obj); 
		      }
		 });
	});
}



