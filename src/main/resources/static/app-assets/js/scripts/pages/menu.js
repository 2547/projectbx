var obj = $(mi);

$(document).ready(function() {
	menu()
	isMobileInit()
})

function isMobileInit() {
	if (isMobile == "true") {
		onClickNavItem()
	}
}

function onClickNavItem() {
	$(".nav-item-children").click(() => {
		$("#loader-222").removeClass("d-none");
		$(".main-menu").addClass("d-none");
	})
}

function menu() {
	if (obj != null) {
		blackBG(obj)
		getListParent(obj);
	}
}

function blackBG(obj) {
	if(obj.parent().hasClass('nav-item')) {
		obj.parent().addClass('active')
	} else {
		obj.parent().addClass('active')
	}
}

function getListParent(obj) {
	if (obj.hasClass('nav-item')) {
		obj.addClass('open')
		return 0;
	}
	return getListParent(obj.parent())
}

function copy(id) {
	var copyText = document.getElementById(id);
	copyText.select();
	document.execCommand("copy");
	Swal.fire(
		'Sucesso!',
		'Copiado  para a área de transferência!',
		'success'
	)
}