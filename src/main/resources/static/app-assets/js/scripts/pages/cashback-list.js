var table;
var baseUrl = "cashback";
var dateFrom = $('#dp-date-range-from')
var dateTo = $('#dp-date-range-to')
var array = [];

$(document).ready(function() {
	table()
	datePickerInitializer()
	validate()
	setInitialDates()
	onProcessingDataTable()
	
})

function hideColumns() {
	if (role == 'CUSTOMER' || role == 'PARTNER')
		array = [7,8,9,10];
}

function setInitialDates() {
	dateFrom.datepicker("setDate", defaultFromDate());
	dateTo.datepicker("setDate", defaultToDate() );
}

function defaultToDate() {
	let date = new Date();
	 return new Date(date.getFullYear(), date.getMonth() + 1, 0);
}

function defaultFromDate() {
	let date = new Date();
	 return new Date(date.getFullYear(), date.getMonth(), 1);
}

function datePickerInitializer() {
    var e = $(".dp-date-range-from").datepicker({
            defaultDate: "+1w",
            dateFormat: "dd/mm/yy",
            changeMonth: !0
        }).on("change", function() {
            t.datepicker("option", "minDate", a(this))
        }),
        t = $(".dp-date-range-to").datepicker({
            defaultDate: "+1w",
            changeMonth: !0,
            dateFormat: "dd/mm/yy",
        }).on("change", function() {
        	
        	// call the api and refresh the data
        	let parsedDate =  a(this)
        	if ($(formId).valid()) {
        		table.ajax.reload();
        	}
            e.datepicker("option", "maxDate", parsedDate);
        });

    function a(e) {
        var t;
        try {
            t = $.datepicker.parseDate('dd/mm/yy', e.value)
        } catch (e) {
            t = null
        }
        return t
    }
    
    $(".ui-datepicker").wrap('<div class="dp-skin"/>')
}

function onProcessingDataTable() {
	table.on( 'processing.dt', function ( e, settings, processing ) {
        $('#cashback-table_processing').removeClass( 'card' );
    } )
}

function table() {
	var url = contextPath + baseUrl + "/getlist";
    table = $(tableId)
        .DataTable({
        	ajax: {
				type: "GET",
				url: url,
				data: {
					dateFrom: function() {
						return dateFrom.datepicker('getDate');
					},
					dateTo: function() {
						return dateTo.datepicker('getDate');
					}
				},
				dataSrc:""
			},
	        processing: true,
            language: getLanguage(),
            columns: getColumns(),
            columnDefs: getColumnDefs(),
            initComplete: function(settings, json) {
            	
                selectTableConfig(this.DataTable());
            },
            dom: 'Bfrtip',
            select: true,
            order: [
                [1, 'asc']
            ]
        });
}

function selectTableConfig(table) {
	genericTableId = (table);
	table.on( 'select deselect', function () {
		 selectDeselectTable(table)
	});
	$(genericTableId + ' tbody').on(
			'dblclick',
			'tr',
			function() {
				table.rows(this).select()
				var obj = table.row({
					selected : true
				}).data();
				window.location.href = contextPath
						+ "/admin/socio/socio?id=" + obj.id;
			});
	
	$(genericTableId).on( 'length.dt', function ( e, settings, newlen ) {
	    len = newlen;
	    table.ajax.reload();
	} );
}

function getTableId (table) {
	return '#'+table.tables().nodes().to$().attr('id');
}

function selectDeselectTable(table) {
	var selectedRows = table.rows( { selected: true } ).count();
    table.button( 1 ).enable( selectedRows > 0 && selectedRows < 2 );
}

function promtpRemove(id) {
	swal({
	    title: "Remover",
	    text: "Tem certeza que deseja remover esse registro?",
	    icon: "warning",
	    buttons: {
            cancel: {
                text: "Não",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Sim",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
	    }
	})
	.then((isConfirm) => {
	    if (isConfirm) {
	    	remove(id)
	    } else {
	    	swal("Cancelado", "Registro não removido!", "error");
	    }
	});
}

function remove(id) {
	$.ajax({
		type: "POST",
		data: {
			id : id
		},
		url : contextPath + "/admin/socio/post/remove",
		success : function(obj) {
			if (obj === true) {
				 table.row({
	                 selected : true
	               }).remove();
				 table.draw();
				swal("Removido!", "Registro removido com sucesso.", "success");
			} else {
				console.log(obj);
				swal("Erro ao Remover!", "Desculpe, mas não foi possível remover esse registro!", "error");
			}
		}
	})
}

function getButtons() {
	return  [ {
		        text: ' <i class="la la-plus font-small-2"></i> Adicionar',
		        className: 'btn-sm btn btn-info round  box-shadow-2 px-2',
		        action: function(e, dt, node, config) {
		            window.location.href = contextPath +
		                "sale/register";
		        },
		        enabled: true
		    }, {
		        text: '<i class="ft-trash-2 font-small-2"></i> Remover ',
		        className : 'btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right',
		        attr:  {
		        	 'data-position':'top', 
		        	 'data-delay':'50',
		        	 'data-tooltip': 'Limpar Formulário'
		        },
		        action: function ( e, dt, node, config ) {
		        	var obj = dt.row( { selected: true } ).data();
		        	promtpRemove(obj.id)
		        },
		        enabled: false
		    }];
}

function formatDate(date) {
	var dd = date.getDate();
	var mm = date.getMonth() + 1; // January is 0!

	var yyyy = date.getFullYear();
	if (dd < 10) {
	  dd = '0' + dd;
	} 
	if (mm < 10) {
	  mm = '0' + mm;
	} 
	return today = dd + '/' + mm + '/' + yyyy;
}

function getColumns() {
	
		    return [{
		        title: "Data Registrada",
		        data: "dateSale",
		        render: function(data, type, full) {
		        	return formatDate(new Date(data));
		        }
		    },
		    
		    {
	            title: "Nome",
	            data: "sale",
	            render: function(data, type, full) {
	            	return data.accountName;
	            }
	        },
		    
		    {
			    title: "Número Documento",
	            data: "sale",
	            render: function(data, type, full) {
	            	return data.documentNo;
	            }
		    }, 
		    
		    {
	            title: "Cidade",
	            data: "sale",
	            render: function(data, type, full) {
	            	return data.cityName;
	            }
	        },
	        {
	            title: "Nível",
	            data: "description"
	        },
	        {
            title: "Valor Compra",
            data: "sale",
            render: function(data, type, full) {
            	return data.saleAmount +"/"+ data.saleCurrency;
            }
        },{
            title: "Valor Cashback",
            data: "amount",
//            render: function(data, type, full) {
//            	return data.cashback +"/"+ data.currency;
//            }
        }, {
            title: "%",
            data: "rate"
        }, {
            title: "País",
            data: "sale",
            render: function(data, type, full) {
            	return data.countryIsoCode;
            }
        },
        
        
        {
            title: "Número da Conta",
            data: "sale",
            render: function(data, type, full) {
            	return data.accountNo;
            }
        }, 
        {
            title: "Nível",
            data: "level"
        }
    ];
}

function getColumnDefs() {
	hideColumns()
	
	return [{
        targets: array,
        visible: false
    },{
        className: "text-capitalize text-truncate text-center",
        targets: "_all"
    }];
}

function validate() {
	$(formId).validate({
		rules : {
			'dp-date-range-from': {
				validDate: true
			},
			'dp-date-range-to': {
				validDate: true
			}
		},
		messages: {},
		errorClass : 'help-block',
		errorElement : 'div',
		success : function(label, element) {
			label.parent().removeClass('error')
		},
		highlight : function(element, errorClass, validClass) {
			$(element).parent().addClass('error')
		},
		errorPlacement : function(error, element) {
		    if ( element.prop('id') === 'entityCode') {
                error.appendTo(element.parent());
            } else {
            	error.insertAfter(element);
            }
		}
	});
}