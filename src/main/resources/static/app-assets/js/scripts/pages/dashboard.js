const accountsDashboard = 'dashboard/getindicatorurl';

$(document).ready(function() {
    getAccount()
    block2(".blockit")
    onClickIndicate()
});

function onClickIndicate() {
    $("#onclick-indicate").click(c => {
        copy("indicate")
    })
}

function getAccount() {
    $.ajax({
        url: accountsDashboard,
        type: "GET",
        success: (obj) => {
            unblock(".blockit")
            $("#indicate").val(obj)
            $(".indicate-href").attr("href", obj);
            $("#indicate-href-whatsapp").attr("href", 'whatsapp://send?text='+obj);
            $("#indicate-href-facebook").attr("href", 'https://facebook.com/sharer/sharer.php?u='+obj);
            
        }
    });
}


