<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<jsp:include page="../../tiles/template/html.jsp"></jsp:include>
<head>
<jsp:include page="../../tiles/template/head.jsp"></jsp:include>
<jsp:include page="../../tiles/template/css.jsp"></jsp:include>
<!-- Select2 -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/app-assets/vendors/css/forms/select/select2.min.css">

<!-- DataTable -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
	<c:choose>
		<c:when test="${isMobile}">
			<jsp:include page="../../tiles/template/header-mobile.jsp"></jsp:include>
			<br />
		</c:when>
		<c:otherwise>
			<jsp:include page="../../tiles/template/header.jsp"></jsp:include>

		</c:otherwise>
	</c:choose>
	<!-- ////////////////////////////////////////////////////////////////////////////-->
	<jsp:include page="../../tiles/template/menu.jsp"></jsp:include>
	<!-- ////////////////////////////////////////////////////////////////////////////-->
	<div class="app-content content">
		<div class="content-wrapper">
			<div class="content-header row">


				<c:choose>
					<c:when test="${isMobile}">
						<br />
					</c:when>
					<c:otherwise>
						<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
							<h3 class="content-header-title mb-0 d-inline-block">${title}</h3>
							<div class="row breadcrumbs-top d-inline-block">
								<div class="breadcrumb-wrapper col-12">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/dashboard">Dashboard</a></li>
									</ol>
								</div>
							</div>
						</div>
					</c:otherwise>
				</c:choose>


			</div>

			<div class="">
				<div class="card" style="">
					<div class="card-header">
						<div>
							<div id="category-list">
								<div class="row match-height">
									<div class="col-xl-4 col-lg-4 col-md-6 sidebar-shop">
										<div class="card">

											<div class="searching btn btn-outline-success">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input" id="cityCheckbox" checked> <label class="custom-control-label" for="cityCheckbox">Deseja pesquisar pela sua cidade?</label>
												</div>
											</div>

											<div class="input-group mb-2 mt-1">
												<input type="text" class="basic-search  form-control" placeholder="Pesquisar" id="query" aria-label="Pesquisar" aria-describedby="basic-addon2">
												<div class="input-group-append">
													<button class="btn btn-success text-white btn-rounded btn-sm my-0 " disabled>
														<i class="la la-search"></i>
													</button>
												</div>
											</div>

										</div>
									</div>
								</div>
								<c:choose>
									<c:when test="${isMobile}">
										<h4 class="card-title mb-1">${title}</h4>
										<a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
									</c:when>
									<c:otherwise>

									</c:otherwise>
								</c:choose>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										<li><a data-action="close"><i class="ft-x"></i></a></li>
									</ul>
								</div>
								<%-- 								<a href="${contextPath}accredited/list?code=${category.code}" class="btn btn-block btn-primary btn-custom btn-social categoria">  --%>
								<%-- 					<i class="la la-institution "></i> <span class="name"> ${category.name}</span> --%>
								<!-- 			     </a> -->

								<div class="card-content collapse show">
									<!-- 										<div class="input-group mb-2 mt-1"> -->
									<!-- 											<input type="text" class="basic-search fuzzy-search form-control" placeholder="Pesquisar" aria-label="Buscar Categoria" aria-describedby="basic-addon2"> -->
									<!-- 											<div class="input-group-append"> -->
									<!-- 												<button class="btn btn-outline-success btn-rounded btn-sm my-0 " disabled> -->
									<!-- 													<i class="la la-search"></i> -->
									<!-- 												</button> -->
									<!-- 											</div> -->
									<!-- 										</div> -->


									<div class="card-content collapse show">

										<div class="list blockit " id="build-list" data-toggle="buttons"></div>

										<div>

											<a class="btn btn-outline-info text-info" role="button" aria-pressed="true" id="filter">Filtrar</a> <a href="${pageContext.request.contextPath}/category/category-list-mobile" class="btn btn-outline-warning float-right text-warning" role="button" aria-pressed="true">Limpar Filtro</a>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ////////////////////////////////////////////////////////////////////////////-->
			<%-- 	<jsp:include page="../../tiles/template/settings.jsp"></jsp:include> --%>
			<c:choose>
				<c:when test="${isMobile}">
					<jsp:include page="../../tiles/template/footer-mobile.jsp"></jsp:include>
					<br />
				</c:when>
				<c:otherwise>
					<jsp:include page="../../tiles/template/footer.jsp"></jsp:include>
				</c:otherwise>
			</c:choose>

			<!-- ///////////////////////////////////js/////////////////////////////////////////-->
			<jsp:include page="../../tiles/template/js.jsp"></jsp:include>
			<script src="${pageContext.request.contextPath}/resources/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
			<jsp:include page="../../tiles/template/alert.jsp"></jsp:include>
			<jsp:include page="../../tiles/template/datatable.jsp"></jsp:include>

			<script src="${pageContext.request.contextPath}/resources/app-assets/vendors/js/tables/datatable/dataTables.select.min.js"></script>
			<script src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/helpers/data-table-helper.js"></script>
			<script src="${pageContext.request.contextPath}/resources/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
			<script src="${pageContext.request.contextPath}/resources/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
			<script src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/pages/${js}"></script>
			<script src="${pageContext.request.contextPath}/resources/app-assets/vendors/js/list/list.min.js"></script>
</body>
</html>
