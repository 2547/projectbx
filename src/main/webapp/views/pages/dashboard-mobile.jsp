<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<jsp:include page="../tiles/template/html.jsp"></jsp:include>
<head>
<meta charset="UTF-8">
<jsp:include page="../tiles/template/head.jsp"></jsp:include>
<jsp:include page="../tiles/template/css.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/app-assets/css/pages/dashboard-mobile.css">
</head>
<body
	class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar"
	data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
	<jsp:include page="../tiles/template/header-mobile.jsp"></jsp:include>
	<!-- ////////////////////////////////////////////////////////////////////////////-->
	<jsp:include page="../tiles/template/menu.jsp"></jsp:include>
	<!-- ////////////////////////////////////////////////////////////////////////////-->

	<div class="app-content content background-mobile">
		<div class="mt-2">
			<div class="col-12">
				<div class="bg-total">
					<div class="card-content ">
						<div class="card-body ">
							<div class="media d-flex">
								<div class="media-body text-center">
									<h4 class="white darken-3 ">Total</h4>
									
									<h3 class=" white font-large-2">${currency} 
									
										<c:choose>
										    <c:when test="${saldo == ''}">
										    0.00
										    </c:when>    
										    <c:otherwise>
										        ${saldo}
										    </c:otherwise>
										</c:choose>
										
										
									</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 bg-white">
						<div class="card blockit" style="box-shadow: none;">
							<div class="card-header pt-0 pr-0 pl-0">

								<!-- 								<a class="heading-elements-toggle" data-action="reload"><i -->
								<!-- 									class="ft-rotate-cw font-medium-3"></i></a> -->
								<!-- 								<div class="heading-elements"> -->
								<!-- 									<ul class="list-inline mb-0"> -->
								<!-- 										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li> -->
								<!-- 									</ul> -->
								<!-- 								</div> -->
								<div class="d-flex justify-content-center">
									<img
										src="${pageContext.request.contextPath}/resources/app-assets/images/ico/icone-app-mmb---indicar.png"
										style="width: 130px;height: 130px;" alt="Compartilhar">
								</div>
								<div>
									<h4 class="card-title text-center">A cada indicado que
										comprar com MyMoneyBox, voc� aumenta seu saldo.</h4>

								</div>
							</div>
							<div class="card-content collapse show">
								<form class="card pl-1 pr-1 mb-1">
									<!-- 									<div class="input-group"> -->
									<!-- 										<input type="text" class="form-control" id="indicate" readonly> -->

									<!-- 									</div> -->

									<div class="row m-0 d-flex justify-content-center mt-1 ">
										<div class="input-group-append mr-1 ml-1">
											<button type="button" class="btn btn-primary"
												id="onclick-indicate">
												<i class="la la-copy"></i>
											</button>
										</div>
										<div class="input-group-append mr-1">
											<a href="" class="btn btn-secondary" id="indicate-href"
												target="_blank"><i class="la la-external-link"></i></a>
										</div>
										<div class="input-group-append mr-1">
											<a target="_blank" class="btn btn-success" href=""
												id="indicate-href-whatsapp"><i class="la la-whatsapp"></i></a>
										</div>

										<div class="input-group-append mr-1">
											<a target="_blank" class="btn btn-info"
												id="indicate-href-facebook" href=""><i
												class="la la-facebook"></i></a>
										</div>
									</div>
									<div class="col-12 text-center text-sm-right pt-2">
											<a href="${pageContext.request.contextPath}/freely/sobre" class="card-link">Saiba mais</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content-wrapper">
				<div class="content-header row mb-1"></div>
				<div class="content-body">
					<!-- Revenue, Hit Rate & Deals -->
					<div class="row">
						<c:forEach items="${stats}" var="stat">
							<div class="col-xl-3 col-lg-6 col-12">
								<div class="card pull-up">
									<div class="card-content">
										<div class="card-body">
											<div class="media d-flex">
												<div class="media-body text-left">
													<h3 class="blue darken-3">${stat.value}</h3>
													<h6>${stat.name}</h6>
												</div>
												<div>
													<i
														class="${stat.icon} purple darken-3 font-large-2 float-right"></i>
												</div>
											</div>
											<div class="progress progress-sm mt-1 mb-0 box-shadow-2">
												<div class="progress-bar bg-gradient-x-purple"
													role="progressbar" style="width: 100%" aria-valuenow="80"
													aria-valuemin="0" aria-valuemax="100"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="../tiles/template/footer-mobile.jsp"></jsp:include>
		<!-- ////////////////////////////////////////////////////////////////////////////-->

		<jsp:include page="../tiles/template/js.jsp"></jsp:include>
		<jsp:include page="../tiles/template/alert.jsp"></jsp:include>
		<script
			src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/pages/menu.js"></script>
		<script
			src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/pages/${js}"></script>
</body>
</html>