<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<jsp:include page="../../tiles/template/html.jsp"></jsp:include>
<head>
<meta charset="UTF-8">
<jsp:include page="../../tiles/template/head.jsp"></jsp:include>
<jsp:include page="../../tiles/template/css.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/app-assets/css/pages/dashboard-mobile.css">
</head>
<body>
<%-- 		<jsp:include page="../../tiles/template/header-mobile.jsp"></jsp:include> --%>
	<!-- ////////////////////////////////////////////////////////////////////////////-->
<%-- 		<jsp:include page="../../tiles/template/menu.jsp"></jsp:include> --%>
	<!-- ////////////////////////////////////////////////////////////////////////////-->
	<jsp:include page="../../tiles/template/loading-mobile.jsp"></jsp:include>
	<nav
		class="navbar navbar-expand-lg navbar-light fixed-navbar background-mobile">
		<a class="navbar-brand text-white nav-item-children"
			href="${pageContext.request.contextPath}/"><i
			class="la la-arrow-left"></i>Voltar</a>

	</nav>
	<div class="d-flex justify-content-center ">
		<img
			src="${pageContext.request.contextPath}/resources/app-assets/images/ico/icone-app-mmb---indicar.png"
			style="width: 130px; height: 130px;" alt="Compartilhar">
	</div>


	<div class="container ">
		<div>
			<h4 class="text-center font-weight-bold">O que � o MMB (My Money Box)</h4>
			<p>O MMB � uma ferramenta moderna, colaborativa e sustent�vel de
				marketing, gerando fideliza��o, engajamento e aumento de convers�o
				para as empresas participantes.</p>
			<p>Seu dinheiro que retorna da sua compra pode ser transferido da
				sua conta virtual para sua conta banc�ria ou tamb�m pode optar pela
				transfer�ncia para seu cart�o de cr�dito pr�-pago internacional</p>
		</div>
		<div>
			<h4 class="text-center font-weight-bold">MMB para Consumidor</h4>
			<p>Custo para ades�o o MMB � ZERO,basta fazer seu cadastro.</p>
			<p>O consumidor recebe uma porcentagem DINHEIRO sobre suas compras e dos seus
				amigos, quanto mais indicar mais GANHA.</p>
			<p>O consumidor recebe os valores do MMB na moeda corrente.
				Podendo ser transferido da sua conta virtual para sua conta banc�ria
				ou tamb�m pode optar pela transfer�ncia para seu cart�o de cr�dito
				pr�-pago internacional.</p>
			<p>O lojista e os consumidores poder�o acessar o sistema e
				acompanhar em tempo real os GANHOS na MMB. O Consumidor recebe um
				SMS confirmando que a compra foi realizada com sucesso.</p>

		</div>
		<div>
			<h4 class="text-center font-weight-bold">MMB para empresas</h4>
			<p>A empresa faz o seu credenciamento a custo ZERO na plataforma,
				define qual ser� a sua pol�tica de porcentagem de remunera��o e
				passa a indicar a plataforma para seus clientes</p>
			<p>O My Money Box passa a fazer a divulga��o de seu
				estabelecimento e suas a��es para TODOS os consumidores cadastrados;</p>
			<p>Os consumidores criam suas contas no MMB e passam a Comprar
				nas lojas credenciadas;</p>
			<p>Um consumidor pode indicar o MMB para outros consumidores e
				assim aumentar o n�mero de cadastros e potenciais convers�es em
				vendas para a empresa parceira;</p>
		</div>
	</div>
	



	<%-- 		<jsp:include page="../../tiles/template/footer-mobile.jsp"></jsp:include> --%>
	<!-- ////////////////////////////////////////////////////////////////////////////-->

	<jsp:include page="../../tiles/template/js.jsp"></jsp:include>
	<jsp:include page="../../tiles/template/alert.jsp"></jsp:include>

	<script
		src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/pages/menu.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/pages/${js}"></script>
</body>
</html>