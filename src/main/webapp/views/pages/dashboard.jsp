<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<jsp:include page="../tiles/template/html.jsp"></jsp:include>
<head>
    <meta charset="UTF-8">
    <jsp:include page="../tiles/template/head.jsp"></jsp:include>
    <jsp:include page="../tiles/template/css.jsp"></jsp:include>
</head>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu-modern" data-col="2-columns">
<jsp:include page="../tiles/template/header.jsp"></jsp:include>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<jsp:include page="../tiles/template/menu.jsp"></jsp:include>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row mb-1"></div>
        <div class="content-body">
            <!-- Revenue, Hit Rate & Deals -->
            <div class="row">
                <c:forEach items="${stats}" var="stat">
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h3 class="blue darken-3">${stat.value}</h3>
                                            <h6>${stat.name}</h6>
                                        </div>
                                        <div>
                                            <i class="${stat.icon} purple darken-3 font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                        <div class="progress-bar bg-gradient-x-purple" role="progressbar"
                                             style="width: 100%" aria-valuenow="80" aria-valuemin="0"
                                             aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div class="row">
                <div class="col-xl-12 col-12">
                    <div class="card blockit">
                        <div class="card-header">
                            <h4 class="card-title">Indicar</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <form class="card p-2">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="indicate" readonly>
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-primary" id="onclick-indicate">Copiar
                                        </button>
                                    </div>
                                    <div class="input-group-append">
                                        <a href="url" class="btn btn-info indicate-href" target="_blank">Abrir</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<jsp:include page="../tiles/template/footer.jsp"></jsp:include>
<jsp:include page="../tiles/template/js.jsp"></jsp:include>
<jsp:include page="../tiles/template/alert.jsp"></jsp:include>
<script src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/pages/menu.js"></script>
<script src="${pageContext.request.contextPath}/resources/app-assets/js/scripts/pages/${js}"></script>
</body>
</html>
