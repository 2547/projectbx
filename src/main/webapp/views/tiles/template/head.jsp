<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="CashBack.">
<meta name="keywords" content="Melhor equipe, sistemas , software, consultoria, cash back, web app, crypto, bitcoin">
<meta name="author" content="Adapta Consultoria">

	
	<meta property="og:site_name" content="My Money Box">
	<meta property="og:url" content="http://app.mymoneybox.com.br">
	<meta property="og:title" content="Mais novo Cash Back, faz seu dinheiro retornar ao seu bolso!">
	<meta property="og:description" content="My Money Box, faz voc� ganhar dinheiro ao mesmo tempo que faz suas compras!">
	<meta property="og:type" content="website">
	<meta property="og:image" content="${pageContext.request.contextPath}/resources/app-assets/images/ico/apple-touch-icon.png"> 
	
	<meta property="og:image" itemprop="image" content="${pageContext.request.contextPath}/resources/app-assets/images/ico/apple-touch-icon.png" />
	
	
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="http://app.mymoneybox.com.br">
	<meta name="twitter:title" content="Mais novo CashBack, faz seu dinheiro retornar ao seu bolso!">
	<meta name="twitter:description" content="My Money Box, faz voc� ganhar dinheiro ao mesmo tempo que faz suas compras!">
	<meta name="twitter:image" content="${pageContext.request.contextPath}/resources/app-assets/images/ico/apple-touch-icon.png">



<link rel="manifest" href="${pageContext.request.contextPath}/resources/app-assets/json/manifest.json">

<link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/resources/app-assets/images/ico/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/resources/app-assets/images/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/resources/app-assets/images/ico/favicon-16x16.png">

<link rel="mask-icon" href="${pageContext.request.contextPath}/resources/app-assets/images/ico/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<title>${page} - ${projectName}</title>
<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/app-assets/images/ico/apple-icon-120.png">
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/app-assets/images/ico/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
<link rel="apple-touch-startup-image" href="${pageContext.request.contextPath}/resources/app-assets/images/ico/apple-icon-120.png" >

