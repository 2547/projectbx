

<div class="bg-footer fixed-bottom" style="opacity: 0.9;">
	<div class="d-flex justify-content-center " style="height: 2.8rem;">
		<a class="w-25 d-flex justify-content-center align-items-center btn btn-float  btn-square btn-primary nav-item-children"
			href="${pageContext.request.contextPath}/dashboard"> <i
			class="la la-home white darken-1 font-large-1"></i>
		</a> 
		
		<a class="w-25 d-flex justify-content-center align-items-center  btn btn-float  btn-square btn-primary nav-item-children"
			href="${pageContext.request.contextPath}/cashback/list"> <span
			class="la la-list-alt white darken-1 font-large-1"></span>
		</a>
		
		<a class="w-25 d-flex justify-content-center align-items-center indicate-href  btn btn-float  btn-square btn-primary nav-item-children"
			href="" > <span
			class="la la-user-plus  white darken-1 font-large-1"></span>
		</a> 
		 <a class="w-25 d-flex justify-content-center align-items-center btn btn-float  btn-square btn-primary nav-item-children"
			href="${pageContext.request.contextPath}/entity/register"> <span
			class="la la-institution white darken-1 font-large-1"></span>
		</a>
		 <a class="w-25 d-flex justify-content-center align-items-center btn btn-float  btn-square btn-primary nav-item-children"
			href="${pageContext.request.contextPath}/category/category-list-mobile"> <span
			class="la la-list white darken-1 font-large-1"></span>
		</a>
	</div>
	
</div>