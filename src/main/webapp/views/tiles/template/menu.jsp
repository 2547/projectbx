<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<div class="main-menu menu-accordion menu-shadow menu-fixed expanded menu-light" data-scroll-to-active="true">
	<div class="main-menu-content">
		<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
			<li class="nav-item nav-item-children"><a id="dashboard" href="${pageContext.request.contextPath}/dashboard"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a> <c:if test="${user.role == 'CUSTOMER'}">



					<li class=" nav-item"><a href="index.html"><i class="la la-clipboard"></i><span class="menu-title" data-i18n="nav.dash.main">Extratos</span></a>
						<ul class="menu-content">

							<li class="nav-item-children"><a class="menu-item" href="${pageContext.request.contextPath}/cashback/listmynetwork"><i></i><span data-i18n="nav.dash.main">Minhas Compras</span></a></li>
							<li class="nav-item-children"><a class="menu-item" href="${pageContext.request.contextPath}/cashback/list"><i></i><span data-i18n="nav.dash.main">Minha Rede</span></a></li>
							
						</ul>
					</li>


					<li class="nav-item nav-item-children"><a id="accredited" href="${pageContext.request.contextPath}/accredited/list"><i class="ft-user-plus"></i><span class="menu-title" data-i18n="nav.dash.main">Lista de Credenciados</span></a>
					<li class="nav-item nav-item-children"><a id="entity" href="${pageContext.request.contextPath}/entity/register"><i class="la la-institution"></i><span class="menu-title" data-i18n="nav.dash.main">Entidade</span></a>
				</c:if>
				
				
				
				 <c:if test="${user.role == 'SELLER'}">
				 
					<li class="nav-item nav-item-children"><a id="saleL" href="${pageContext.request.contextPath}/sale/register"><i class="la la-cart-plus"></i><span class="menu-title" data-i18n="nav.dash.main">Registrar Compra</span></a>
					<li class="nav-item nav-item-children"><a id="saleList" href="${pageContext.request.contextPath}/sale/list"><i class="la la-list"></i><span class="menu-title" data-i18n="nav.dash.main">Lista de Compras</span></a>
					
				</c:if>
				
				 <c:if test="${user.role == 'PARTNER'}">
					<li class="nav-item nav-item-children"><a id="partner" href="${pageContext.request.contextPath}/partner/register"><i class="ft-users"></i><span class="menu-title" data-i18n="nav.dash.main">Registrar Parceiro</span></a>
					<li class="nav-item nav-item-children"><a id="accredited" href="${pageContext.request.contextPath}/accredited/register"><i class="ft-user-plus"></i><span class="menu-title" data-i18n="nav.dash.main">Registrar Credenciado</span></a>
					
					<li class=" nav-item"><a href="index.html"><i class="la la-clipboard"></i><span class="menu-title" data-i18n="nav.dash.main">Extratos</span></a>
						<ul class="menu-content">
							<li class="nav-item-children"><a class="menu-item" href="${pageContext.request.contextPath}/cashback/listmynetwork"><i></i><span data-i18n="nav.dash.main">Minhas Compras</span></a></li>
							<li class="nav-item-children"><a class="menu-item" href="${pageContext.request.contextPath}/cashback/list"><i></i><span data-i18n="nav.dash.main">Minha Rede</span></a></li>
						</ul>
					</li>
					
					
					
<%-- 					<li class="nav-item nav-item-children"><a id="cashback" href="${pageContext.request.contextPath}/cashback/list"><i class="ft-clipboard"></i><span class="menu-title" data-i18n="nav.dash.main">Extrato Cashback</span></a>  --%>
					
					<c:choose>
							<c:when test="${isMobile}">
								<li class="nav-item nav-item-children"><a href="${pageContext.request.contextPath}/category/category-list-mobile"><i class="la la-list"></i><span class="" data-i18n="nav.dash.main">Categorias</span></a></li>
								
							</c:when>
							<c:otherwise>

							</c:otherwise>
					</c:choose>
				</c:if>
				<c:choose>
							<c:when test="${isMobile}">
								
								<li class="nav-item"><a href="https://api.whatsapp.com/send?phone=5504588307521"><i class="la la-whatsapp"></i><span class="" data-i18n="nav.dash.main">Fale Conosco</span></a></li>
							</c:when>
							<c:otherwise>

							</c:otherwise>
					</c:choose>
		</ul>
	</div>
</div>





<%-- 	<li class="nav-item"><a id="dashboard" href="${pageContext.request.contextPath}/dashboard"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a> --%>
<%-- 			<li class="nav-item"><a id="accredited" href="${pageContext.request.contextPath}/accredited/list"><i class="ft-user-plus"></i><span class="menu-title" data-i18n="nav.dash.main">Registrar Credenciado</span></a> --%>
<%-- 			<li class="nav-item"><a id="partner" href="${pageContext.request.contextPath}/partner/register"><i class="ft-users"></i><span class="menu-title" data-i18n="nav.dash.main">Registrar Parceiro</span></a> --%>
<%-- 			<li class="nav-item"><a id="sale" href="${pageContext.request.contextPath}/sale/list"><i class="la la-cart-plus"></i><span class="menu-title" data-i18n="nav.dash.main">Registrar Compra</span></a> --%>
<%-- 			<li class="nav-item"><a id="cashback" href="${pageContext.request.contextPath}/cashback/list"><i class="ft-clipboard"></i><span class="menu-title" data-i18n="nav.dash.main">Extrato Cashback</span></a> --%>











