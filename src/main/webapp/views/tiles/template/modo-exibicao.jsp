<ul class="list-inline mb-0">
	<li class="float-right"><a data-action="collapse"><i
			class="ft-minus"></i></a></li>
	<li class="float-right mr-1"><a data-action="reload"><i
			class="ft-rotate-cw"></i></a></li>
	<li class="float-right mr-1"><a data-action="expand"><i
			class="ft-maximize"></i></a></li>
	<!-- <li><a data-action="close"><i class="ft-x"></i></a></li> -->
</ul>